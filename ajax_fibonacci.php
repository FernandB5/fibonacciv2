<?php
/**
 * File Name: ajax_fibonacci
 * Description: Do the Fibonacci alghorithm at server side
 * Version: 1.0
 * Author: Battisti Fernand
 * 
 */


class nombre
{
    private $resultNb;
    private $actualNb;
    private $precedentNb;
    private $numberMax;
    public function __construct($numberMax) 
    {
        $this->numberMax = $numberMax;
        $this->resultNb = 1;
        $this->actualNb = 1;
        $this->precedentNb=0;
    }
    public function getNumber() 
    {
        return $this->numberMax;
    }
    public function getResultNb() 
    {
        return $this->resultNb;
    }
    public function getActualNb() 
    {
        return $this->actualNb;
    }

    public function getPrecedentNb() 
    {
        return $this->precedentNb;
    }
    public function setResultNb($nb) 
    {
        $this->resultNb = $nb;
    }
    public function setActualNb($nb) 
    {
        return $this->actualNb = $nb;
    }
    public function setPrecedentNb($nb) 
    {
        return $this->precedentNb = $nb;
    }

    function fibonacci()
    {
        $arrayResults=array();
        $arrayResults["avant"]=$this->getNumber();
        
        if ($this->getNumber() > 0)
        {   
        $i = 0;
        
            while ($i < $this->getNumber()-2)
            { 
                $this->setPrecedentNb($this->getActualNb() );
                $this->setActualNb($this->getResultNb());
                $this->setResultNb($this->getActualNb()+ $this->getPrecedentNb());
                $i++;
            }


            
        }
        $arrayResults["finale"]=$this->getResultNb();

        return json_encode($arrayResults);
    }
}




if(!empty($_POST['number']))
{
    
    $n=strip_tags(trim($_POST['number']));
    if(is_numeric($n))
    {
        $nombre=new nombre($n);
        echo $nombre->fibonacci();
    }
    else
    echo "error";
}
else
{
    echo "error";
}
    
