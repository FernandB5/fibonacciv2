

jQuery(function() 
{
  
});
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function doFibonacci()
{
    if($("#saisie").val().toString().length>0&&isNumeric($("#saisie").val())&&$("#saisie").val()>0)
    {
        if(!$(".alert-warning").hasClass("hidden"))
        $(".alert-warning").addClass("hidden");

        $.post("ajax_fibonacci.php", {number: $("#saisie").val()}, function(result)
        {
            console.log(result);
            var array0=JSON.parse(result);
            if(result!="error")
            {
                if(!$(".alert-warning").hasClass("hidden"))
                $(".alert-warning").addClass("hidden");
                
                $("#valSaisie").html(array0["avant"]);
               
                $("#valFinale").html(array0["finale"]);
            }
            else
            {
                alert("Une erreur est survenue...veuillez m'excuser du désagrément !");
                $(".alert-warning").removeClass("hidden");
            }
        });
    }
    else
    {
        if($(".alert-warning").hasClass("hidden")&&$("#saisie").val().toString().length>0&&(!isNumeric($("#saisie").val())||$("#saisie").val()<=0))
        $(".alert-warning").removeClass("hidden");
    }
};