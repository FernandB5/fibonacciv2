<!DOCTYPE html>
<html lang="en">
<head>
  <title>Fibonacci</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="doFibonacci.js"></script>
  <link rel="stylesheet" href="style.css"/> 
</head>
<body>
    <div id=imgTop class=" col-xs-12 col-md-12 col-sm-offset-2 col-lg-offset-2 col-sm-8 col-lg-8">
        <img alt=introduction class=img-responsive id=imgTop width=100% src="https://battapps.ch/wp-content/uploads/2020/11/f0qiomwdp-o-scaled.jpg"/>
    </div>
    <h1 id="title" class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-sm-offset-3 col-lg-offset-3 col-sm-6 col-lg-6">Fibonacci</h1>
    <input onkeyup="doFibonacci()"type=text placeholder="Saisissez n>0..." id="saisie" class="col-xs-offset-4 col-xs-4 col-md-offset-4 text-center col-md-4 col-sm-offset-4 col-lg-offset-5 col-sm-3 col-lg-2"/>
    <div class=row></div>
    <div class=container-fluid>
      <div  class="text-center titreDiv col-xs-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-offset-2 col-xs-4 col-md-4 col-sm-4 col-lg-4"><h1 id=titre1 class=titre>Valeur saisie<h1><h3 id=valSaisie class=chiffre ><h3></div>
      <div  class="text-center titreDiv col-xs-4 col-md-4 col-sm-4 col-lg-4"><h1 id=titre3 class=titre>Résultat<h1><h3 id=valFinale class=chiffre ><h3></div>
      
    </div>
    <h2 class="row hidden text-center alert alert-warning ">La saisie n'est pas valide, veuillez entrer un nombre, qui soit plus grand que 0 ! </h2>
  </body>
</html>
